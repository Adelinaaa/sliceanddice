class Carousel {
  slides = [];
  indicators = [];
  next;
  prev;
  interval;
  currentSlide = 0;
  duration = 2000;
  constructor(element) {
    this.slides = document.querySelectorAll(element + " .slide");
    this.indicators = document.querySelectorAll(element + " .indicator");
    this.next = document.querySelector(element + " .next");
    this.prev = document.querySelector(element + " .prev");
    this.showSlide(this.currentSlide);
    this.interval = setInterval(this.nextSlide, this.duration);
    setTimeout(() => this.attachEventListeners(), 1000);
  }

  showSlide(idx) {
    if (idx > this.slides.length - 1) {
      idx = 0;
    }
    if (idx < 0) {
      idx = this.slides.length - 1;
    }
    this.currentSlide = idx;
    this.hideSlides();
    this.clearIndicators();
    this.slides[idx].classList.add("active");
    if (this.indicators[idx]) {
      this.indicators[idx].classList.add("active");
    }
  }

  hideSlides() {
    for (var i = 0; i < this.slides.length; i++) {
      this.slides[i].classList.remove("active");
    }
  }

  clearIndicators() {
    for (let i = 0; i < this.indicators.length; i++) {
      this.indicators[i].classList.remove("active");
    }
  }
  nextSlide = () => {
    this.showSlide(this.currentSlide + 1);
  };

  prevSlide = () => {
    this.showSlide(this.currentSlide - 1);
  };

  attachEventListeners() {
    if (this.next) {
      this.next.addEventListener("click", () => {
        this.nextSlide();
        this.resetInterval();
      });
    }

    if (this.prev) {
      this.prev.addEventListener("click", () => {
        this.prevSlide();
        this.resetInterval();
      });
    }

    if (this.indicators) {
      for (let i = 0; i < this.indicators.length; i++) {
        this.indicators[i].addEventListener("click", () => {
            this.showSlide(i);
            this.resetInterval();
        });
      }
    }
  }

  resetInterval() {
    clearInterval(this.interval);
    this.interval = setInterval(this.nextSlide, this.duration);
  }
}
