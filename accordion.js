let accordions = document.getElementsByClassName("accordion");

for (let i = 0; i < accordions.length; i++) {
  accordions[i].addEventListener("click", function () {
    let icon = this.getElementsByClassName("arrow-icon")[0];
    icon.classList.toggle("rotate");

    this.classList.add("active");
    let panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = "100px";
    }

    for (let j = 0; j < accordions.length; j++) {
      if (j != i) {
        accordions[j].classList.remove("active");
        accordions[j].nextElementSibling.style.maxHeight = null;
        accordions[j].children[1].children[0].classList.remove("rotate");
      }
    }
  });
}
