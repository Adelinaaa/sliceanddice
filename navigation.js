let navExpander = document.querySelector('.expand-nav');

navExpander.addEventListener("click", () => {
  var x = document.querySelector(".toggle-nav");
  let y = document.querySelector(".main-carousel");
  if (x.className === "toggle-nav") {
    x.className += " responsive";
    y.className += " responsive"
  } else {
    x.className = "toggle-nav";
    y.className += ".main-carousel"
  }
})

window.addEventListener("scroll", function() {
  let header = document.querySelector("header");
  header.classList.toggle("sticky", window.scrollY > 0)
})